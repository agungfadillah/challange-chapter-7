const passport = require('../lib/passport')

module.exports = (req, res, next) => {
    if(req.isAuthenticated()) {
        return next()
    }

    return res.redirect('auth/login')
}

module.exports = passport.authenticate('jwt', {
    session: false
})