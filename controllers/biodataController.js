const {Biodata} = require('../models')

module.exports = {
    index: async (req, res) => {
        const biodata = await Biodata.findAll()
        res.json(biodata)
        res.render('data/tabel')
    },
    show: (req, res) => {
        Biodata.findOne({
            where: {
                id: req.params.id
            }
        }).then((biodata) => {
            res.json(biodata)
            res.render('data/tabel')
        })
    },
    create: async(req, res) =>{
        const {kode_user, nama, alamat, telp, status, tgl_lahir} = req.body
        const biodata = await Biodata.create({
            kode_user: req.body.kode_user,
            nama: req.body.nama,
            alamat: req.body.alamat,
            telp: req.body.telp,
            status: req.body.status,
            tgl_lahir: req.body.tgl_lahir,
        })
        res.status(201).json(biodata)
        res.render('data/admin')
    },
    edit: async(req, res) => {
        const id = req.params.id

        const biodata = await Biodata.update({
            alamat: req.body.alamat,
            telp: req.body.telp,
            status: req.body.status
        }, {
            where: {
                id: id
            }
        })
        res.render('/admin')
        res.json({
            messege: 'Updated Biodata'
        })
    },
    delete: async (req, res) =>{
        const id = req.params.id

        const biodata = await Biodata.destroy({
            where: {
                id: id
            }
        })
        res.render('/')
        res.json({
            messege: 'Deleted Biodata'
        })
    }
}
