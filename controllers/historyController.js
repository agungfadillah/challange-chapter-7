const {History} = require('../models')

module.exports = {
    index: async (req, res) => {
        const history = await History.findAll()
        res.json(history)
    },
    show: (req, res) => {
        History.findOne({
            where: {
                id: req.params.id
            }
        }).then((history) => {
            res.json(history)
        })
    },
    create: async(req, res) =>{
        const history = await History.create({
            kode_user: req.body.kode_user,
            username: req.body.username,
            skor: req.body.skor,
            last_game: req.body.last_game
        })
        res.status(201).json(history)
        res.render('tabel')
    },
    edit: async(req, res) => {
        const id = req.params.id

        const history = await History.update({
            kode_user: req.body.kode_user,
            username: req.body.username,
            skor: req.body.skor,
            last_game: req.body.last_game
        }, {
        where: {
            id: id
        }
    })

        res.json({
            messege: 'Updated History'
        })
    },
    delete: async (req, res) =>{
        const id = req.params.id

        const history = await History.destroy({
            where: {
                id: id
            }
        })
        res.json({
            messege: 'Deleted History'
        })
    }
}

