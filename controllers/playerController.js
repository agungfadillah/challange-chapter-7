const {Player} = require('../models')

module.exports = {
    index: async (req, res) => {
        const player = await Player.findAll()
        res.json(player)
    },
    show: (req, res) => {
        Player.findOne({
            where: {
                id: req.params.id
            }
        }).then((player) => {
            res.json(player)
        })
    },
    create: async(req, res) =>{
        const players = await Player.create({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
        })
        
        res.status(201).json(players)
    },
    edit: async(req, res) => {
        const id = req.params.id

        const players = await Player.update({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
        }, {
            where: {
                id: id
            }
        })
        res.json({
            message: 'Updated Player'
        })
    },
    delete: async (req, res) =>{
        const id = req.params.id

        const players = await Player.destroy({
            where: {
                id:id
            }
        })
        res.json({
            message: 'Deleted Player'
        })
    }
}

