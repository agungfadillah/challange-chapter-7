const {User} = require('../models')
const passport = require('../lib/passport')

function format(user){
    const {id, username} = user

    return{
        id,
        username,
        accessToken: user.generateToken()
    }
}

module.exports = {
    registerForm: (req, res) => {
        res.render('register')
    },
    register: (req, res, next) => {
        User.register(req.body).then(() => {
            res.redirect('/login')
        }).catch(err => {
            next(err)
        })
    },
    loginForm: (req, res) => {
        res.render('login')
    },
    login : async (req, res) => {
        try{
            const user = await User.authenticate(req.body)
            res.json(format(user))
        }catch(e){
            res.json(403, {
                message: 'Login failed'
            })
        }
    }
}
