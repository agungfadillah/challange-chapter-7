const path = require('path')
const express = require('express')
const ejs = require('ejs')
const app = express();
const bodyParser = require('body-parser')
const port = 3000
const client = require('./connect')


app.use(express.urlencoded({extended: false}))
app.use(express.json())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

app.use(express.static('public'))
app.use('/css', express.static(__dirname + 'public/css'))
app.use('/images', express.static(__dirname + 'public/images'))
app.use('/assets', express.static(__dirname + 'public/assets'))
app.use('/javascript', express.static(__dirname + 'public/javascript'))

const passport = require('./lib/passport')
app.use(passport.initialize())

app.set('views', './views')
app.set('view engine', 'ejs')


app.use((request, response, next ) => {
    console.log(`${request.method} ${request.url}`)
    next()
})

const router = require('./routes/route');
app.use('/', router)


const authRouter = require("./routes/auth.js")
app.use("/auth", authRouter)


app.use((err, req, res, next) => {
    res.status(500).json({
        status: ' fail',
        errors: err.message
    })
})

app.use((req, res, next) => {
    res.status(404).json({
        status: 'fail',
        error: 'Are you lost?'
    })
})

app.listen(port, () => {
    console.log(`Web started at port : ${port}`)
})

client.connect(err => {
    if(err){
        console.log(err.message)
    } else {
        console.log('connected')
    }
})