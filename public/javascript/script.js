function botRandom() {
    const bot = Math.floor(Math.random() * 3);
    if (bot==0) {
    com="batu";
    document.getElementById("batu2").style.background=
    "#C4C4C4";
    document.getElementById("gunting2").style.background=
    "#9C835F";
    document.getElementById("kertas2").style.background=
    "#9C835F";
    }
    else if (bot==1) {
    com="gunting";
    document.getElementById("gunting2").style.background=
    "#C4C4C4";
    document.getElementById("kertas2").style.background=
    "#9C835F";
    document.getElementById("batu2").style.background=
    "#9C835F";
    } else {
    com="kertas";
    document.getElementById("kertas2").style.background=
    "#C4C4C4";
    document.getElementById("gunting2").style.background=
    "#9C835F";
    document.getElementById("batu2").style.background=
    "#9C835F";
    }
}
function batu(){
    document.getElementById("batu2").style.background=
        "#C4C4C4";
    document.getElementById("gunting2").style.background=
        "#9C835F";
    document.getElementById("kertas2").style.background=
        "#9C835F";
    player="batu";
    botRandom();
    winHasil();
    matchResult();
}

function gunting(){
    document.getElementById("gunting2").style.background=
    "#C4C4C4";
    document.getElementById("kertas2").style.background=
    "#9C835F";
    document.getElementById("batu2").style.background=
    "#9C835F";
    player="gunting";
    botRandom();
    winHasil();
    matchResult();
}

function kertas(){
    document.getElementById("kertas2").style.background=
    "#C4C4C4";
    document.getElementById("gunting2").style.background=
    "#9C835F";
    document.getElementById("batu2").style.background=
    "#9C835F";
    player="kertas";
    botRandom();
    winHasil();
    matchResult();
}

function winHasil(){
    if (player==com){
        hasil="Draw";
    } else if(player=="batu" && com=="gunting"){
        hasil="Player WIN";
    } else if(player=="gunting" && com=="kertas"){
        hasil="Player WIN";
    } else if(player=="kertas" && com=="batu"){
        hasil="Player WIN";
    } else{
        hasil="COM WIN";
    }
}

function matchResult() {
        if(hasil == "Draw"){
            let replace = document.querySelector(".replace")
            replace.innerHTML="DRAW"
            replace.classList.add("result")
            replace.style.background ="#035B0C"
            if (replace.classList.contain("versus")){ 
                replace.classList.remove("versus")
            }
            console.log("Match Draw")
      } else if(hasil=="Player WIN"){
            let replace = document.querySelector(".replace")
            replace.innerHTML="PLAYER 1 <br> WIN"
            replace.classList.add("result")
            replace.style.background = "#4C9654"
            if (replace.classList.contain("versus")){
                replace.classList.remove("versus")
            }
            console.log("Player Win")
      } else {
            let replace = document.querySelector(".replace")
            replace.innerHTML="COM <br> WIN"
            replace.classList.add("result")
            replace.style.background = "#4C9654"
            if (replace.classList.contain("versus")){
                replace.classList.remove("versus")
            }
            console.log("Com Win")
        }
      }
    