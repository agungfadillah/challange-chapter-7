const express = require('express')
const router = express()
const {Biodata} = require('../models')
const playerController = require('../controllers/playerController')
const biodataController = require('../controllers/biodataController')
const historyController = require('../controllers/historyController')

router.use(express.json())


router.get("/home", (req, res) => {
    res.render('home')
})

router.get("/game", (req, res) => {
    res.render('game')
})

router.get("/register", (req, res) => {
    res.render('register')
})

router.get("/login", (req, res) => {
    res.render('login')
})

router.get("/admin", (req, res) => {
    res.render('admin')
})

router.get("/tabel", (req, res) => {
    res.render('tabel')
})

router.get("/select", (req, res) => {
    res.render('select')
})

//Route player
router.get('/player', playerController.index)
router.get('/player/:id', playerController.show)
router.post('/player', playerController.create)
router.put('/player/:id', playerController.edit)
router.delete('/player/:id', playerController.delete)

//Route history
router.get('/histories', historyController.index)
router.get('/histories/:id', historyController.show)
router.post('/histories', historyController.create)
router.put('/histories/:id', historyController.edit)
router.delete('/histories/:id', historyController.delete)

//Route biodata
router.get('/biodata', biodataController.index)
router.get('/biodata/:id', biodataController.show)
router.post('/biodata', biodataController.create)
router.put('/biodata/:id', biodataController.edit)
router.delete('/biodata/:id', biodataController.delete)

router.get('/biodata', (req, res) => {
    res.render('/tabel')
})

router.get('/biodata', (req, res) => {
    Biodata.findAll()
        .then(biodata => {
            res.render('/tabel', { biodata })
        })
})

router.get('/biodata', (req, res) => {
    res.render('biodata')
})

router.post('/biodata', (req, res) => {
    const { kode_user, nama, tgl_lahir, alamat, telp, status } = req.body;
    Biodata.create({ kode_user, nama, tgl_lahir, alamat, telp, status })
        .then(biodata => {
            res.redirect(`/biodata/`)
        })
        .catch(err => {
            res.send(`Gagal membuat artikel, karena ${JSON.stringify(err.message, null, 2)}`)
        })
})

module.exports = router