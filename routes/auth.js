const express = require('express')
const router = express.Router()
const {Player} = require('../models')
const {Biodata} = require('../models')
const { response } = require('./route')


//route level middleware
router.use((request, response, next) => {
    console.log('router level middleware')
    next()
})
router.get('/login', (request, response) => {
    response.render('login')
})
router.post('/login', (request, response) => {

    const {email, password} = request.body
    Player.findOne({
        where: {
            email: email
        }
    })

    users.push({email, password})

    response.redirect('/game')
})
router.get('/register', (request, response) => {
    response.render('auth/login')
})
router.post('/register', (request, response) => {
    const {email, password} = request.body

    users.push({email, password})

    response.redirect('auth/login')
})


module.exports = router